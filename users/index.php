
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewpoint" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA_Compatible" content="IE=edge">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>





    <title>Project Name</title>

    <link rel="stylesheet" href="../css/master.css">
    <link rel="icon" type="../image/png" href="img/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="../image/png" href="img/favicon-16x16.png" sizes="16x16" />


    <script type="text/javascript">

    var loading = document.createElement('img');
    loading.setAttribute("src", "../files/loading.gif");




      // Loads at start

      $(function() {
        // console.clear();
          if ($('#ourResults_table').length == 1) {
              enableDisableButtons(true, false);
          } else {

            enableDisableButtons(true, true);
          }
          document.getElementById('earthData').disabled = true;
          document.getElementById('ourResults').disabled = true;

      });












      // Disables and Enables buttons

      function enableDisableButtons(a,b) {
        document.getElementById('submitBtn').disabled = a;
        document.getElementById('clearBtn').disabled = b;
      }



     function submitSearch() {

       $('#ourResults_table').remove();






// "https://bypasscors.herokuapp.com/api/?url=" with url allows CORS bypass (temp fix)

      let url = "https://bypasscors.herokuapp.com/api/?url=https://earthdata.nasa.gov/search?q=" + document.getElementById('search').value;
      let results = $("#earthDataResults").load(url + " .result a.ext");
      document.getElementById('earthDataResults').style.display = "none";



      if (document.getElementById('loading').style.visibility == 'hidden') {
        document.getElementById('loading').style.visibility = 'visible';
      }


      document.getElementById('loading').appendChild(loading);

      window.setTimeout(showEarthDataResults, 3000);




    } // END OF sumbitSearch function







     function showEarthDataResults() {

       document.getElementById('loading').style.visibility = 'hidden';


       let rowCount = $("#earthDataResults a").length;
       $('#earthData').html("Earth Data Results - " + rowCount + " results");

       // Puts all the links in a nice table
       let ext = document.getElementsByClassName('ext');
       $("a").wrap("<tr></tr>");
       $("a").wrap("<td></td>");
       for (var i = 0; i < ext.length; i++) {

         ext[i].style.color = "white";

       }


         document.getElementById('ourResults').disabled = false;


       if (rowCount != 0) {
         document.getElementById('earthData').disabled = false;
       }
     }


     function earthDataResults() {
       $("#earthDataResults").fadeIn();
       document.getElementById('earthDataResults').style.display = "inline-table";
       document.getElementById('earthDataResults').style.width = "50%";
       document.getElementById('earthDataResults').style.marginLeft = "25%";
       document.getElementById('earthDataResults').style.marginTop = "2%";
       document.getElementById('earthDataResults').style.borderRadius = "25px";


     }


     function ourResults() {
       $('#search_form').submit(function (e) {
         e.preventDefault();
         e.stopPropagation();
       });
       // The rest in in the table becuase PHP doesn't liek being chagned after it's made.

     }




      // Search box plus sumbit/clear button enable/disable

      function Search(e) {

        let searchBoxCheck = document.getElementById('search').value;



        if (searchBoxCheck == '') {
         enableDisableButtons(true,true);


        }
        else {
          enableDisableButtons(false,false);

        }

        if(e.keyCode == 13) {
          submitSearch();
        }


      }


      // Clears Page to default

      function Clear() {
        let loading = document.createElement('img');
        loading.setAttribute("src", "../files/loading.gif");
        document.getElementById('search').value = '';
        enableDisableButtons(true,true);
        $('#earthData').html("Earth Data Results");
        document.getElementById('ourResults').disabled = true;
        document.getElementById('earthData').disabled = true;
        $('#earthDataResults').html("");
        $('#ourResults').html("ADST Results");
        $('#ourResults_table').remove();
      }


      // makes advance search table show and hide

      function showAdvancedSearch(){

        let hiddenTable = document.getElementById('advanceSearch');

        if (hiddenTable.style.display == "inline-block") {
            hiddenTable.style.display = "none";

        }
        else {
          hiddenTable.style.display = "inline-block";


        }








      }





    </script>



<!-- beginning of html body -->



  </head>
  <body>
        <!-- banner -->
        <div class="banner">
          <p id="projectName">Project Name</p>
          <img src="../img/banner.png" alt="Space The Final Frontier" style="width:100%;">
        </div>


<!-- for submission later -->
<form class="Search" id="search_form" action="<?=$_SERVER['PHP_SELF'];?>" method="post">


        <!-- search bar -->
        <table class="searchBar" >
          <tr>

          <td width="100%">
            <input id="search" type="text" name="search" placeholder="Search" style="width:98%;line-height: 2em;" onkeyup="Search(event);">
          </td>

          <td width="5%">

            <button id="submitBtn" class="btn btn-success" type="button" name="submit" onclick="submitSearch();">Submit</button>

          </td>

          <td width="5%">
            <button id="clearBtn" type="button" class="btn btn-secondary" name="clear" onclick="Clear()">Clear</button>
          </td>
          <td id="advSearch">
              <a href="javascript:void(0);" onclick="showAdvancedSearch()">advance search</a>
          </td>


        </tr>


       </table> <!-- end of searchBar  -->

      <!-- advance search table  -->
      <table id="advanceSearch" >



        <tr>

          <td id="advanceSearchBoxes">
            <span>Mission: </span>

                <select name="mission">
                  <option value="INTEX-A" selected>INTEX-A</option>
                  <option value="INTEX-B">INTEX-B</option>
                  <option value="TexasAQS">TexasAQS</option>
                  <option value="DC3">DC3</option>
                  <option value="SEAC4RS">SEAC4RS</option>
                </select>
          </td>
          <td id="advanceSearchBoxes" colspan="2">
            <span>Platform:</span>
            <select name="platform">
              <option value="DC8" selected>DC8</option>
              <option value="NP3">NP3</option>
              <option value="Falcon">Falcon</option>
              <option value="GV">GV</option>
            </select>
          </td>
          <td id="advanceSearchBoxes">
            <span>Researcher First Name:</span>
              <input type="text" name="researcherFName" size="12px" placeholder="Brennen">
          </td>
          <td id="advanceSearchBoxes">
            <span>Last Name:</span>
            <input type="text" name="researcherLName" size="12px" placeholder="Murphy">
          </td>
          <td id="advanceSearchBoxes">
            <span>Year from: </span>
              <input type="date" name="yearMin" min="1900">
              <span> To: </span>
              <input type="date" name="yearMax" min="1900">
          </td>

          <td id="advanceSearchBoxes">
              <span>Instrument:</span>
              <select name="instrument">
                <option value="TDL" selected>TDL</option>
                <option value="WS-CRDS">WS-CRDS</option>
                <option value="Optical">Optical</option>
                <option value="NON-O3-Chemiluminescence">NON-O3-Chemiluminescence</option>
                <option value="NO-NO2">NO-NO2</option>
                <option value="NOyO3">NOyO3</option>
              </select>

          </td>

        </tr>


       </table> <!--end of advance search -->

 <br />

      <!-- search buttons -->
       <table id="searchButtons">

      <tr>
        <td><button type="submit" class="btn btn-info" id="ourResults" name="ourResults" onclick="ourResults();">ADST Results</button></td>
        <td><button type="button" class="btn btn-info" id="earthData" name="earthData" onclick="earthDataResults();">Earth Data Results</button></td>
        <td><button type="button" class="btn btn-info" id="odiseesData" name="odiseesData" onclick="odiseesData();" disabled>Odisses Results (W.I.P)</button></td>
     </tr>

     </table>

     <!-- W.I.P -->
     <table id="ourResults_table" class="table table-dark" style="display: inline-table; width: 50%; margin-left: 25%; margin-top: 2%; border-radius: 25px;">

       <?php


        if (strtoupper($_SERVER['REQUEST_METHOD']) === 'POST') {
          require('../dbtest/get_results.php');
            global $r;
            $r = get_our_results($_POST['search']);



            if (!empty($r)) {


            foreach($r as $rows) {
              foreach ($rows as $key => $value) {
                echo "<tr><th>" . $key . "</th></tr><tr><td>" .$value . "</td></tr>";
              }
            }
            unset($rows);
          }

        }



       ?>






   </table>

      <!-- displays the search results from earth data for now. -->
      <table id="earthDataResults" class="table table-dark">





    </table>


    <table id="odiseesResults">




  </table>


  <div id="loading"></div>


  </form>







  </body>
</html>
