<!DOCTYPE html>
<?
  include_once('../dbtest/dconn.php');
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Add Data to the Database</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->


      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>




      <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
      <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>



      <link rel="stylesheet" href="../css/ingest.css">


          <script type="text/javascript">




          // Will display the 'Top' button opon scrolling past 400px from top of screen

          window.onscroll = function() {scrollFunction();};

          function scrollFunction() {
            if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
              document.getElementById("scrollUp").style.display = "block";
              document.getElementById("scrollUpv2").style.display = "block";
            } else {
              document.getElementById("scrollUp").style.display = "none";
              document.getElementById("scrollUpv2").style.display = "none";
            }
          }

          // When the user clicks on the button, scroll to the top of the document
          function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
          }





















// checks to see if form is valid

          function isValidForm() {


            let projectName = document.forms["validCheck"]["projectName"].value;
            let instrumentPILastName = document.forms["validCheck"]["instrumentPILastName"].value;
            let instrumentPIFirstName = document.forms["validCheck"]["instrumentPIFirstName"].value;
            if(projectName == "" || instrumentPILastName == "" || instrumentPIFirstName == "") {
              let f = document.getElementById('vailidateForm');
              f.innerHTML = 'Form not filled out all the way!';
              f.style.display = 'block';


            $('input').each(function() {
              if($(this).prop('required')) {
                $(this).addClass('check');
              }
            });



              return false;




            }
            else {
                return true;
            }


          }








          </script>




  </head>
  <body>

    <ul class="nav nav-tabs justify-content-center">
      <li class="nav-item"><a data-toggle="tab" class="nav-link active" href="#projectInfo">Project Info</a></li>
      <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#platformInfo">Platform Info</a></li>
      <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#platformSamplingST_Info">Platform Sampling</a></li>
      <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#scienceTeam">Science Team Info</a></li>
      <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#dataSourceInfo">Data Source Info</a></li>
      <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#dataCollection_Info">Data Collection Info</a></li>
      <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#dataFileInfo">Data File Info</a></li>
      <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#dataVarInfo">Data Variable Info</a></li>
      <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#verify">Verify</a></li>

    </ul>
    <br />









 <!-- Project Info Tab -->



    <div class="tab-content">
      <div class="tab-pane active" id="projectInfo">
          <div class="jumbotron container-fluid">

    <form action="<?=htmlentities('../dbtest/ingest_db.php');?>" method="post" id="validFormCheck" name="validCheck" onsubmit="return isValidForm()">
            <div class="form-group">
              <label class="col-sm-3 col-form-label" for="projectName">Project Name<span id='astrisk'> *</span></label>
              <input type="text" class="form-control mb-2 mr-sm-2" name="projectName" placeholder="Project Name" required>
              <br>
              <label class="col-sm-3 col-form-label" for="projectAcronym">Project Acronym</label>
              <input type="text" class="form-control" name="projectAcronym" placeholder="Project Acronym" >
              <br>
              <label class="col-sm-3 col-form-label" for="projectPI">Project PI</label>
              <input type="text" class="form-control" name="projectPI" placeholder="Project PI" >
              <br>
              <label class="col-sm-3 col-form-label" for="projectDescription">Project Description</label>
              <input type="text" class="form-control" name="projectDescription" placeholder="Project Description" >
              <br>
              <label class="col-sm-3 col-form-label" for="deploymentLocation">Deployment Location</label>
              <input type="text" class="form-control" name="deploymentLocation" placeholder="Deployment Location" >
              <br>
              <label class="col-sm-3 col-form-label" for="deploymentStartDate">Deployment Start Date<span id='astrisk'> *</span></label>
              <input type="date" class="form-control" name="deploymentStartDate" required>
              <br>
              <label class="col-sm-3 col-form-label" for="deploymentEndDate">Deployment End Date<span id='astrisk'> *</span></label>
              <input type="date" class="form-control" name="deploymentEndDate" required>
            </div>




        </div>

      </div>


<!-- Platform Info tab -->


      <div class="tab-pane fade" id="platformInfo">
        <div class="jumbotron container-fluid">



          <div class="form-group">
            <label class="col-sm-3 col-form-label" for="platformName">Platform Name<span id='astrisk'> *</span></label>
            <input type="text" class="form-control mb-2 mr-sm-2" name="platformName" placeholder="Platform Name" required>
            <br>
            <label class="col-sm-3 col-form-label" for="platformAcronym">Platform Acronym</label>
            <input type="text" class="form-control" name="platformAcronym" placeholder="Platform Acronym" >
            <br>
            <label class="col-sm-3 col-form-label" for="platformIdentifier">Platform Identifier</label>
            <input type="text" class="form-control" name="platformIdentifier" placeholder="Aircraft tailer number or ground site code, ...">
            <br>
            <label class="col-sm-3 col-form-label" for="platformType">Platform Type</label>
            <input type="text" class="form-control" name="platformType" placeholder="Aircraft, Ships, Vehicles, Ground Sites, and Sondes">
            <br>
            <label class="col-sm-3 col-form-label" for="platformOperationStartDate">Platform Operation Start Date<span id='astrisk'> *</span></label>
            <input type="date" class="form-control" name="platformOperationStartDate" required>
            <br>
            <label class="col-sm-3 col-form-label" for="platformOperationEndDate">Platform Operation End Date<span id='astrisk'> *</span></label>
            <input type="date" class="form-control" name="platformOperationEndDate" required>
          </div>




      </div>
      </div>





<!-- Platform Sampling tab -->

      <div class="tab-pane fade" id="platformSamplingST_Info">
        <div class="jumbotron container-fluid">


          <div class="form-group">
            <label class="col-sm-3 col-form-label" for="samplingStartDateUTC">Sampling Start Date UTC<span id='astrisk'> *</span></label>
            <input type="date" class="form-control mb-2 mr-sm-2" name="samplingStartDateUTC" required>
            <br>
            <label class="col-sm-3 col-form-label" for="samplingStartTimeUTC">Sampling Start Time UTC<span id='astrisk'> *</span></label>
            <input type="text" date-format="HH:mm" pattern="([01]?[0-9]|2[0-3]):[0-5][0-9]" class="form-control" name="samplingStartTimeUTC" placeholder="HH:mm" required>
            <br>
            <label class="col-sm-3 col-form-label" for="samplingEndDateUTC">Sampling End Date UTC<span id='astrisk'> *</span></label>
            <input type="date" class="form-control" name="samplingEndDateUTC" placeholder="Sampling End Date UTC" required>
            <br>
            <label class="col-sm-3 col-form-label" for="samplingEndTimeUTC">Sampling End Time UTC<span id='astrisk'> *</span></label>
            <input type="text" date-format="HH:mm" pattern="([01]?[0-9]|2[0-3]):[0-5][0-9]" class="form-control" name="samplingEndTimeUTC" placeholder="HH:mm" required>
            <br>
            <label class="col-sm-3 col-form-label" for="sampling_StartLocation">Sampling Start Location</label>
            <input type="text" class="form-control" name="samplingStartLocation" placeholder="Only for mobile platforms: airport (lat, lon), ship sampling stations (lat, lon), and mobile van (lat, lon)">
            <br>
            <label class="col-sm-3 col-form-label" for="sampling_EndLocation">Sampling End Location</label>
            <input type="text" class="form-control" name="samplingEndLocation" placeholder="Only for mobile platforms: airport (lat, lon), ship sampling stations (lat, lon), and mobile van (lat, lon)">
            <br>
            <label class="col-sm-3 col-form-label" for="samplingSpatialRepresentationType">Sampling Spatial Representation Type</label>
            <input type="text" class="form-control" name="samplingSpatialRepresentationType" placeholder="Polygon,  box, flight track - line simplification">
            <br>
            <label class="col-sm-3 col-form-label" for="samplingSpatialExtent">Sampling Spatial Extent</label>
            <input type="text" class="form-control" name="samplingSpatialExtent" placeholder="Location for ground sites (lat, lon, elevation), sampling region for mobile platforms">
          </div>




      </div>
      </div>



<!-- Science Team Info tab -->


<div class="tab-pane fade" id="scienceTeam">
  <div class="jumbotron container-fluid">


    <div class="form-group">
      <label class="col-sm-3 col-form-label" for="instrumentPILastName">Instrument PI Last Name<span id='astrisk'> *</span></label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="instrumentPILastName" placeholder="Doe" required>
      <br>
      <label class="col-sm-3 col-form-label" for="instrumentPIFirstName">Instrument PI First Name<span id='astrisk'> *</span></label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="instrumentPIFirstName" placeholder="John" required>
      <br>
      <label class="col-sm-3 col-form-label" for="instrumentPIMiddleName">Instrument PI Middle Name</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="instrumentPIMiddleName" placeholder="Lee">
      <br>
      <label class="col-sm-3 col-form-label" for="instrumentPIAffiliationInstitution">Instrument PI Affiliation Institution</label>
      <input type="text" class="form-control" name="instrumentPIAffiliationInstitution" placeholder="NASA LaRC">
      <br>
      <label class="col-sm-3 col-form-label" for="instrumentPIAffiliationCity">Instrument PI Affiliation City</label>
      <input type="text" class="form-control" name="instrumentPIAffiliationCity" placeholder="Hampton">
      <br>
      <label class="col-sm-3 col-form-label" for="instrumentPIAffiliationCountry">Instrument PI Affiliation Country</label>
      <input type="text" class="form-control" name="instrumentPIAffiliationCountry" placeholder="USA">
      <br>
      <label class="col-sm-3 col-form-label" for="instrumentPIContactInfoEmail">Instrument PI Contact Info Email<span id='astrisk'> *</span></label>
      <input type="email" class="form-control" name="instrumentPIContactInfoEmail" placeholder="john.l.doe@nasa.gov" required>
      <br>
      <label class="col-sm-3 col-form-label" for="instrumentPIContactInfoPhone">Instrument PI Contact Info Phone</label>
      <input type="tel" class="form-control" name="instrumentPIContactInfoPhone" placeholder="555-555-5555">
      <br>
      <label class="col-sm-3 col-form-label" for="instrumentPIActivePeriodStartDate">Instrument PI Active Period Start Date<span id='astrisk'> *</span></label>
      <input type="date" class="form-control" name="instrumentPIActivePeriodStartDate" required>
      <br>
      <label class="col-sm-3 col-form-label" for="instrumentPIActivePeriodEndDate">Instrument PI Active Period End Date<span id='astrisk'> *</span></label>
      <input type="date" class="form-control" name="instrumentPIActivePeriodEndDate" required>
    </div>




</div>
</div>






<!-- Data Source Info tab -->


<div class="tab-pane fade" id="dataSourceInfo">
  <div class="jumbotron container-fluid">



    <div class="form-group">
      <label class="col-sm-3 col-form-label" for="dataSourceOrigin">Data Soruce Origin</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataSoruceOrigin" placeholder="Manufacture, developer">
      <br>
      <label class="col-sm-3 col-form-label" for="dataSourceIdentifier">Data Source Identifier</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataSourceIdentifier" placeholder="Model Number and Serial Number/Development Date/version number">
    </div>


</div>
</div>





<!-- Data Collection Info tab -->


<div class="tab-pane fade" id="dataCollection_Info">
  <div class="jumbotron container-fluid">


    <div class="form-group">
      <label class="col-sm-3 col-form-label" for="dataCollectionName">Data Collection Name</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataCollectionName" placeholder="NAAMES-LARGE-APS">
      <br>
      <label class="col-sm-3 col-form-label" for="dataProductType">Data Product Type</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataProductType" placeholder="Slant colum, vertical column, profile, in-situ, curtain...">
      <br>
      <label class="col-sm-3 col-form-label" for="dataFormatType">Data Format Type</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataFormatType" placeholder="ICARTT, HDF5, netCDF4...">
      <br>
      <label class="col-sm-3 col-form-label" for="samplingLocationSource">Sampling Location Source</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="samplingLocationSource" placeholder="Nav data location">
      <br>
      <label class="col-sm-3 col-form-label" for="lastRevisionDate">Last Revision Date<span id='astrisk'> *</span></label>
      <input type="date" class="form-control mb-2 mr-sm-2" name="lastRevisionDate" required>
      <br>
      <label class="col-sm-3 col-form-label" for="fileBeingModified">File Being Modified</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="fileBeingModified" placeholder="If files are modified to have consistent number of variables and variable names">
      <br>
      <label class="col-sm-3 col-form-label" for="missingFileDates">Missing File Dates</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="missingFileDates" placeholder="Identify the flight when there was no data">
      <br>
      <label class="col-sm-3 col-form-label" for="numberOfMissingFiles">Missing File Dates</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="numberOfMissingFiles" placeholder="Try to match up with GCMDa">
      <br>
      <label class="col-sm-3 col-form-label" for="pIVariable">PI Variables</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="pIVariable" placeholder="List variables under each category">
    </div>



</div>
</div>








<!-- Data File Info tab -->


<div class="tab-pane fade" id="dataFileInfo">
  <div class="jumbotron container-fluid">


    <div class="form-group">


      <!-- GO TO TOP BUTTON -->
      <button onclick="topFunction();" type="button" id="scrollUp" title="Go to top">Top</button>

      <label class="col-sm-3 col-form-label" for="dataFileName">Data File Name</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataFileName" placeholder="Data File Name">
      <br>
      <label class="col-sm-3 col-form-label" for="dataFileVolume">Data File Volume</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataFileVolume" placeholder="Deal with mutiple flights in one day">
      <br>
      <label class="col-sm-3 col-form-label" for="dataFile_NumOfFileVolumes">Data File Number Of File Volumes<span id='astrisk'> *</span></label>
      <input type="number" class="form-control mb-2 mr-sm-2" name="dataFileNumberOfFileVolumes" placeholder="0" min="0" required>
      <br>
      <label class="col-sm-3 col-form-label" for="dataFileWorkingPath">Data File Working Path</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataFileWorkingPath" placeholder="Working Directory?">
      <br>
      <label class="col-sm-3 col-form-label" for="dataFileNullDataFlag">Data File Null Data Flag</label>

      <!-- Making a fake bool for postgreSQL -->
      <select class="form-control mb-2 mr-sm-2" name="dataFileNullDataFlag">
        <option selected value="0">No</option>
        <option value="1">Yes</option>
      </select>

      <br>
      <label class="col-sm-3 col-form-label" for="dataCollectionStartDateUTC">Data Collection Start Date UTC<span id='astrisk'> *</span></label>
      <input type="date" class="form-control mb-2 mr-sm-2" name="dataCollectionStartDateUTC" placeholder="Data Collection Start Date UTC" required>
      <br>
      <label class="col-sm-3 col-form-label" for="dataFileRevisionNum">Data File Revision Number</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataFileRevisionNum" placeholder="Data File Revision Number">
      <br>
      <label class="col-sm-3 col-form-label" for="dataFileRevisionComment">Data File Revision Comment</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataFileRevisionComment" placeholder="Data File Revision Comment">
      <br>
      <label class="col-sm-3 col-form-label" for="dataFileRevisionDateUTC">Data File Revision Date UTC<span id='astrisk'> *</span></label>
      <input type="date" class="form-control mb-2 mr-sm-2" name="dataFileRevisionDateUTC" placeholder="Data File Revision Date UTC" required>
      <br>
      <label class="col-sm-3 col-form-label" for="dataFileDataStartTimeUTC">Data File Data Start Time UTC<span id='astrisk'> *</span></label>
      <input type="text" date-format="HH:mm" pattern="([01]?[0-9]|2[0-3]):[0-5][0-9]" class="form-control mb-2 mr-sm-2" name="dataFileDataStartTimeUTC" placeholder="HH:mm" required>
      <br>
      <label class="col-sm-3 col-form-label" for="dataFileDataEndTimeUTC">Data File Data End Time UTC<span id='astrisk'> *</span></label>
      <input type="text" date-format="HH:mm" pattern="([01]?[0-9]|2[0-3]):[0-5][0-9]" class="form-control mb-2 mr-sm-2" name="dataFileDataEndTimeUTC" placeholder="HH:mm" required>
      <br>
      <label class="col-sm-3 col-form-label" for="dataFile_DataInterval">Data File Data Interval</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataFileDataInterval" placeholder="Data File Data Interval">
      <br>
      <label class="col-sm-3 col-form-label" for="dataFile_StartTime_Label">Data File Start Time Label</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataFileStartTimeLabel" placeholder="Data File Start Time Label">
      <br>
      <label class="col-sm-3 col-form-label" for="dataFile_StopTime_Label">Data File Stop Time Label</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataFileStopTimeLabel" placeholder="Data File Stop Time Label">
      <br>
      <label class="col-sm-3 col-form-label" for="sataFile_MidTime_Label">Data File Mid Time Label</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataFileMidTimeLabel" placeholder="Data File Mid Time Label">
      <br>
      <label class="col-sm-3 col-form-label" for="dataFile_NumOfNormalCommentLines">Data File Number Of Normal Comment Lines<span id='astrisk'> *</span></label>
      <input type="number" min="0" class="form-control mb-2 mr-sm-2" name="dataFileNumOfNormalCommentLines" placeholder="0" required>
      <br>
      <label class="col-sm-3 col-form-label" for="dataFile_NumOfSpecialCommentLines">Data File Number of Special Comment Lines<span id='astrisk'> *</span></label>
      <input type="number" min="0" class="form-control mb-2 mr-sm-2" name="dataFileNumOfSpecialCommentLines" placeholder="0" required>

    </div>






</div>
</div>















<!-- Data Variable Info tab -->


<div class="tab-pane fade" id="dataVarInfo">
  <div class="jumbotron container-fluid">



 <!-- GO TO TOP BUTTON -->
 <button onclick="topFunction();" type="button" id="scrollUpv2" title="Go to top">Top</button>

    <div class="form-group">
      <label class="col-sm-3 col-form-label" for="dataVariableName">Data Variable Name<span id='astrisk'> *</span></label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableName" placeholder="PI variable name" required>
      <br>
      <label class="col-sm-3 col-form-label" for="dataVariableDescription">Data Variable Description</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableDescription" placeholder="Data Variable Description">
      <br>
      <label class="col-sm-3 col-form-label" for="dataVariableType">Data Variable Type</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableType" placeholder="Scalar, vector, flag, array...">
      <br>
      <label class="col-sm-3 col-form-label" for="dataVariableStandardNameCore">Data Variable Standard Name Core</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableStandardNameCore" placeholder="Data Variable Standard Name Core">
      <br>
      <label class="col-sm-3 col-form-label" for="dataVariableStandardNameFull">Data Variable Standard Name Full</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableStandardNameFull" placeholder="This should be consistent with the description">
      <br>
      <label class="col-sm-3 col-form-label" for="dataVariableStandardNameIndex">Data Variable Standard Name Index</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableStandardNameIndex" placeholder="Data Variable Standard Name Index">
      <br>
      <label class="col-sm-3 col-form-label" for="dataVariableUNCLabel">Data Variable UNC Label</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableUNCLabel" placeholder="Uncertainty">
      <br>
      <label class="col-sm-3 col-form-label" for="dataVariableLLODLabel">Data Variable LLOD Label</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableLLODLabel" placeholder="Data Variable LLOD Label">
      <br>
      <label class="col-sm-3 col-form-label" for="dataVariableMissingDataFlag">Data Variable Missing Data Flag</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableMissingDataFlag" placeholder="Data Variable Missing Data Flag">
      <br>
      <label class="col-sm-3 col-form-label" for="dataVariableLLODataFlag">Data Variable LLOD Data Flag</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableLLODdataFlag" placeholder="Data Variable LLOD Data Flag">
      <br>
      <label class="col-sm-3 col-form-label" for="dataVariableLLODValue">Data Variable LLOD Value</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableLLODValue" placeholder="Data Variable LLOD Value">
      <br>
      <label class="col-sm-3 col-form-label" for="dataVariableULODValue">Data Variable ULOD Value</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableULODValue" placeholder="Data Variable ULOD Value">
      <br>
      <label class="col-sm-3 col-form-label" for="dataVariableDataFlagLabel">Data Variable Data Flag Label</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableDataFlagLabel" placeholder="Data Variable Data Flag Label">
      <br>
      <label class="col-sm-3 col-form-label" for="dataVariableDataFlagType">Data Variable Data Flag Type</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableDataFlagType" placeholder="Data Variable Data Flag Type">
      <br>
      <label class="col-sm-3 col-form-label" for="sataFile_MidTime_Label">Data File Mid Time Label</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataFileMidTimeLabel" placeholder="Data File Mid Time Label">
      <br>
      <label class="col-sm-3 col-form-label" for="dataVariable_OutputName">Data Variable Output Name</label>
      <input type="text" class="form-control mb-2 mr-sm-2" name="dataVariableOutputName" placeholder="Data Variable Output Name">
    </div>


</div>
</div>








<!-- Verify tab -->


<div class="tab-pane fade" id="verify">
  <div class="jumbotron container-fluid">



      <div class="">

      </div>

      <label class="col-sm-12 col-form-label text-center" id="verifyLabel" for="submit">You verify that all previous info is accurate and correct</label>
      <br><br>
      <button type="submit" class="btn btn-outline-success btn-block" onclick="isValidForm()">Submit</button>

<br><br>
      <div class="alert alert-danger" id="vailidateForm" style="display:none;text-align:center"></div>


  </form>

</div>
</div>









    </div>


  </body>
</html>
