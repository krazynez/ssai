<?php
require('dbconn.php');



// Project Info Tab
$projectName = filter_input(INPUT_POST, 'projectName');
$projectAcronym = filter_input(INPUT_POST, 'projectAcronym');
$projectPI = filter_input(INPUT_POST, 'projectPI');
$projectDescription = filter_input(INPUT_POST, 'projectDescription');
$deploymentLocation = filter_input(INPUT_POST, 'deploymentLocation');
$deploymentStartDate = filter_input(INPUT_POST, 'deploymentStartDate');
$deploymentEndDate = filter_input(INPUT_POST, 'deploymentEndDate');





// Platform Info Tab
$platformName = filter_input(INPUT_POST, 'platformName');
$platformAcronym = filter_input(INPUT_POST, 'platformAcronym');
$platformIdentifier = filter_input(INPUT_POST, 'platformIdentifier');
$platformType = filter_input(INPUT_POST, 'platformType');
$platformOperationStartDate = filter_input(INPUT_POST, 'platformOperationStartDate');
$platformOperationEndDate = filter_input(INPUT_POST, 'platformOperationEndDate');

// Platform Sampling Tab
$samplingStartDateUTC = filter_input(INPUT_POST, 'samplingStartDateUTC');
$samplingStartTimeUTC = filter_input(INPUT_POST, 'samplingStartTimeUTC');
$samplingEndDateUTC = filter_input(INPUT_POST, 'samplingEndDateUTC');
$samplingEndTimeUTC = filter_input(INPUT_POST, 'samplingEndTimeUTC');
$samplingStartLocation = filter_input(INPUT_POST, 'samplingStartLocation');
$samplingEndLocation = filter_input(INPUT_POST, 'samplingEndLocation');
$samplingSpatialRepresentationType = filter_input(INPUT_POST, 'samplingSpatialRepresentationType');
$samplingSpatialExtent = filter_input(INPUT_POST, 'samplingSpatialExtent');


//science Team Info Tab
$instrumentPILastName = filter_input(INPUT_POST, 'instrumentPILastName');
$instrumentPIFirstName = filter_input(INPUT_POST, 'instrumentPIFirstName');
$instrumentPIMiddleName = filter_input(INPUT_POST, 'instrumentPIMiddleName');
$instrumentPIAffiliationInstitution = filter_input(INPUT_POST, 'instrumentPIAffiliationInstitution');
$instrumentPIAffiliationCity = filter_input(INPUT_POST, 'instrumentPIAffiliationCity');
$instrumentPIAffiliationCountry = filter_input(INPUT_POST, 'instrumentPIAffiliationCountry');
$instrumentPIContactInfoEmail = filter_input(INPUT_POST, 'instrumentPIContactInfoEmail', FILTER_VALIDATE_EMAIL);
$instrumentPIContactInfoPhone = filter_input(INPUT_POST, 'instrumentPIContactInfoPhone');
$instrumentPIActivePeriodStartDate = filter_input(INPUT_POST, 'instrumentPIActivePeriodStartDate');
$instrumentPIActivePeriodEndDate = filter_input(INPUT_POST, 'instrumentPIActivePeriodEndDate');


// Data Source Info Tab
$dataSourceOrigin = filter_input(INPUT_POST, 'dataSourceOrigin');
$dataSourceIdentifier = filter_input(INPUT_POST, 'dataSourceIdentifier');


// Data Collection Info
$dataCollectionName = filter_input(INPUT_POST, 'dataCollectionName');
$dataProductType = filter_input(INPUT_POST, 'dataProductType');
$dataFormatType = filter_input(INPUT_POST, 'dataFormatType');
$samplingLocationSource = filter_input(INPUT_POST, 'samplingLocationSource');
$lastRevisionDate = filter_input(INPUT_POST, 'lastRevisionDate');
$fileBeingModified = filter_input(INPUT_POST, 'fileBeingModified', FILTER_VALIDATE_BOOLEAN);

// Fix for fake bool
  if ($fileBeingModified == '') {
    $fileBeingModified = 0;
  }

$missingFileDates = filter_input(INPUT_POST, 'missingFileDates');
$numberOfMissingFiles = filter_input(INPUT_POST, 'numberOfMissingFiles', FILTER_VALIDATE_INT);

// Fix for fake bool
  if ($numberOfMissingFiles == '') {
    $numberOfMissingFiles = 0;
  }

$pIVariable = filter_input(INPUT_POST, 'pIVariable');

// Data File Info
$dataFileName = filter_input(INPUT_POST, 'dataFileName');
$dataFileVolume = filter_input(INPUT_POST, 'dataFileVolume');
$dataFileNumberOfFileVolumes = filter_input(INPUT_POST, 'dataFileNumberOfFileVolumes', FILTER_VALIDATE_INT);

// Fix for fake bool
  if ($dataFileNumberOfFileVolumes == '') {
    $dataFileNumberOfFileVolumes = 0;
  }

$dataFileWorkingPath = filter_input(INPUT_POST, 'dataFileWorkingPath');
$dataFileNullDataFlag = filter_input(INPUT_POST, 'dataFileNullDataFlag', FILTER_VALIDATE_INT);
$dataCollectionStartDateUTC = filter_input(INPUT_POST, 'dataCollectionStartDateUTC');
$dataFileRevisionNum = filter_input(INPUT_POST, 'dataFileRevisionNum');
$dataFileRevisionComment = filter_input(INPUT_POST, 'dataFileRevisionComment');
$dataFileRevisionDateUTC = filter_input(INPUT_POST, 'dataFileRevisionDateUTC');
$dataFileDataStartTimeUTC = filter_input(INPUT_POST, 'dataFileDataStartTimeUTC');
$dataFileDataEndTimeUTC = filter_input(INPUT_POST, 'dataFileDataEndTimeUTC');
$dataFileDataInterval = filter_input(INPUT_POST, 'dataFileDataInterval');
$dataFileStartTimeLabel = filter_input(INPUT_POST, 'dataFileStartTimeLabel');
$dataFileStopTimeLabel = filter_input(INPUT_POST, 'dataFileStopTimeLabel');
$dataFileMidTimeLabel = filter_input(INPUT_POST, 'dataFileMidTimeLabel');
$dataFileNumberOfNormalCommentLines = filter_input(INPUT_POST, 'dataFileNumOfNormalCommentLines', FILTER_VALIDATE_INT);

// Fix for fake bool
if ($dataFileNumberOfNormalCommentLines == '') {
  $dataFileNumberOfNormalCommentLines = 0;
}

// Fix for fake bool
$dataFileNumberOfSpecialCommentLines = filter_input(INPUT_POST, 'dataFileNumOfSpecialCommentLines', FILTER_VALIDATE_INT);

if ($dataFileNumberOfSpecialCommentLines == '') {
  $dataFileNumberOfSpecialCommentLines = 0;
}



// Data Variable Info
$dataVariableName = filter_input(INPUT_POST, 'dataVariableName');
$dataVariableDescription = filter_input(INPUT_POST, 'dataVariableDescription');
$dataVariableType = filter_input(INPUT_POST, 'dataVariableType');
$dataVariableStandardNameCore = filter_input(INPUT_POST, 'dataVariableStandardNameCore');
$dataVariableStandardNameFull = filter_input(INPUT_POST, 'dataVariableStandardNameFull');
$dataVariableStandardNameIndex = filter_input(INPUT_POST, 'dataVariableStandardNameIndex');
$dataVariableUNCLabel = filter_input(INPUT_POST, 'dataVariableUNCLabel');
$dataVariableLLODLabel = filter_input(INPUT_POST, 'dataVariableLLODLabel');
$dataVariableMissingDataFlag = filter_input(INPUT_POST, 'dataVariableMissingDataFlag', FILTER_VALIDATE_INT);

// Fix for fake bool
  if ($dataVariableMissingDataFlag == '') {
    $dataVariableMissingDataFlag = 0;
  }


$dataVariableLLODdataFlag = filter_input(INPUT_POST, 'dataVariableLLODdataFlag', FILTER_VALIDATE_INT);

// Fix for fake bool
  if ($dataVariableLLODdataFlag == '') {
    $dataVariableLLODdataFlag = 0;
  }

$dataVariableLLODValue = filter_input(INPUT_POST, 'dataVariableLLODValue');
$dataVariableULODValue = filter_input(INPUT_POST, 'dataVariableULODValue');
$dataVariableDataFlagLabel = filter_input(INPUT_POST, 'dataVariableDataFlagLabel');
$dataVariableDataFlagType = filter_input(INPUT_POST, 'dataVariableDataFlagType');
$dataFileMidTimeLabel = filter_input(INPUT_POST, 'dataFileMidTimeLabel');
$dataVariableOutputName = filter_input(INPUT_POST, 'dataVariableOutputName');




// Querying it all together.... yay... fun times...

// PROJECT INFO QUERY



$projectInfoTabQuery = $dbconn->prepare("INSERT INTO project_info (projectname, projectacronym, projectpi, projectdescription, deploymentlocation, deploymentstartdate, deploymentenddate) VALUES (:projectName, :projectAcronym, :projectPI, :projectDescription, :deploymentLocation, :deploymentStartDate, :deploymentEndDate)");
$projectInfoTabQuery->bindParam(':projectName', $projectName);
$projectInfoTabQuery->bindParam(':projectAcronym', $projectAcronym);
$projectInfoTabQuery->bindParam(':projectPI', $projectPI);
$projectInfoTabQuery->bindParam(':projectDescription', $projectDescription);
$projectInfoTabQuery->bindParam(':deploymentLocation', $deploymentLocation);
$projectInfoTabQuery->bindParam(':deploymentStartDate', $deploymentStartDate);
$projectInfoTabQuery->bindParam(':deploymentEndDate', $deploymentEndDate);
$projectInfoTabQuery->execute();




// PLATFORM INFO QUERY
$platformInfoTabQuery = $dbconn->prepare("INSERT INTO platform_info (platformname, platformacronym, platformidentifier, platformtype, platformoperationstartdate, platformoperationenddate) VALUES (:platformName, :platformAcronym, :platformIdentifier, :platformType, :platformOperationStartDate, :platformOperationEndDate)");
$platformInfoTabQuery->bindParam(':platformName', $platformName);
$platformInfoTabQuery->bindParam(':platformAcronym', $platformAcronym);
$platformInfoTabQuery->bindParam(':platformIdentifier', $platformIdentifier);
$platformInfoTabQuery->bindParam(':platformType', $platformType);
$platformInfoTabQuery->bindParam(':platformOperationStartDate', $platformOperationStartDate);
$platformInfoTabQuery->bindParam(':platformOperationEndDate', $platformOperationEndDate);
$platformInfoTabQuery->execute();



// PLATFORM SAMPLING QUERY
$platformSamplingTabQuery = $dbconn->prepare("INSERT INTO platform_sampling (samplingstartdateutc, samplingstarttimeutc, samplingenddateutc, samplingendtimeutc, samplingstartlocation, samplingendlocation, samplingspatialrepresentationtype, samplingspatialextent) VALUES (:samplingStartDateUTC, :samplingStartTimeUTC, :samplingEndDateUTC, :samplingEndTimeUTC, :samplingStartLocation, :samplingEndLocation, :samplingSpatialRepresentationType, :samplingSpatialExtent)");
$platformSamplingTabQuery->bindParam(':samplingStartDateUTC', $samplingStartDateUTC);
$platformSamplingTabQuery->bindParam(':samplingStartTimeUTC', $samplingStartTimeUTC);
$platformSamplingTabQuery->bindParam(':samplingEndDateUTC', $samplingEndDateUTC);
$platformSamplingTabQuery->bindParam(':samplingEndTimeUTC', $samplingEndTimeUTC);
$platformSamplingTabQuery->bindParam(':samplingStartLocation', $samplingStartLocation);
$platformSamplingTabQuery->bindParam(':samplingEndLocation', $samplingEndLocation);
$platformSamplingTabQuery->bindParam(':samplingSpatialRepresentationType', $samplingSpatialRepresentationType);
$platformSamplingTabQuery->bindParam(':samplingSpatialExtent', $samplingSpatialExtent);
$platformSamplingTabQuery->execute();



// SCIENCE TEAM INFO QUERY
$scienceTeamInfoTabQuery = $dbconn->prepare("INSERT INTO science_team_info (instrumentpilastname, instrumentpifirstname, instrumentpimiddlename, instrumentpiaffiliationinstitution, instrumentpiaffiliationcity, instrumentpiaffiliationcountry, instrumentpicontactinfoemail, instrumentpicontactinfophone, instrumentpiactiveperiodstartdate, instrumentpiactiveperiodendDate) VALUES (:instrumentPILastName, :instrumentPIFirstName, :instrumentPIMiddleName, :instrumentPIAffiliationInstitution, :instrumentPIAffiliationCity, :instrumentPIAffiliationCountry, :instrumentPIContactInfoEmail, :instrumentPIContactInfoPhone, :instrumentPIActivePeriodStartDate, :instrumentPIActivePeriodEndDate)");
$scienceTeamInfoTabQuery->bindParam(':instrumentPILastName', $instrumentPILastName);
$scienceTeamInfoTabQuery->bindParam(':instrumentPIFirstName', $instrumentPIFirstName);
$scienceTeamInfoTabQuery->bindParam(':instrumentPIMiddleName', $instrumentPIMiddleName);
$scienceTeamInfoTabQuery->bindParam(':instrumentPIAffiliationInstitution', $instrumentPIAffiliationInstitution);
$scienceTeamInfoTabQuery->bindParam(':instrumentPIAffiliationCity', $instrumentPIAffiliationCity);
$scienceTeamInfoTabQuery->bindParam(':instrumentPIAffiliationCountry', $instrumentPIAffiliationCountry);
$scienceTeamInfoTabQuery->bindParam(':instrumentPIContactInfoEmail', $instrumentPIContactInfoEmail);
$scienceTeamInfoTabQuery->bindParam(':instrumentPIContactInfoPhone', $instrumentPIContactInfoPhone);
$scienceTeamInfoTabQuery->bindParam(':instrumentPIActivePeriodStartDate', $instrumentPIActivePeriodStartDate);
$scienceTeamInfoTabQuery->bindParam(':instrumentPIActivePeriodEndDate', $instrumentPIActivePeriodEndDate);
$scienceTeamInfoTabQuery->execute();

// DATA SOURCE INFO QUERY
$dataSourceInfoTabQuery = $dbconn->prepare("INSERT INTO data_source_info (datasourceorigin, datasourceidentifier) VALUES (:dataSourceOrigin, :dataSourceIdentifier)");
$dataSourceInfoTabQuery->bindParam(':dataSourceOrigin', $dataSourceOrigin);
$dataSourceInfoTabQuery->bindParam(':dataSourceIdentifier', $dataSourceIdentifier);
$dataSourceInfoTabQuery->execute();


// DATA COLLECTION INFO QUERY
$dataCollectionInfoTabQuery = $dbconn->prepare("INSERT INTO data_collection_info (datacollectionname, dataproducttype, dataformattype, samplinglocationsource, lastrevisiondate, filebeingmodified, missingfiledates, numberofmissingfiles, pivariable) VALUES (:dataCollectionName, :dataProductType, :dataFormatType, :samplingLocationSource, :lastRevisionDate, :fileBeingModified, :missingFileDates, :numberOfMissingFiles, :pIVariable)");
$dataCollectionInfoTabQuery->bindParam(':dataCollectionName', $dataCollectionName);
$dataCollectionInfoTabQuery->bindParam(':dataProductType', $dataProductType);
$dataCollectionInfoTabQuery->bindParam(':dataFormatType', $dataFormatType);
$dataCollectionInfoTabQuery->bindParam(':samplingLocationSource', $samplingLocationSource);
$dataCollectionInfoTabQuery->bindParam(':lastRevisionDate', $lastRevisionDate);
$dataCollectionInfoTabQuery->bindParam(':fileBeingModified', $fileBeingModified);
$dataCollectionInfoTabQuery->bindParam(':missingFileDates', $missingFileDates);
$dataCollectionInfoTabQuery->bindParam(':numberOfMissingFiles', $numberOfMissingFiles);
$dataCollectionInfoTabQuery->bindParam(':pIVariable', $pIVariable);
$dataCollectionInfoTabQuery->execute();

// DATA FILE INFO QUERY
$dataFileInfoTabQuery = $dbconn->prepare("INSERT INTO data_file_info (datafilename, datafilevolume, datafilenumberoffilevolumes, datafileworkingpath, datafilenulldataflag, datacollectionstartdateutc, datafilerevisionnumber, datafilerevisioncomment, datafilerevisiondateutc, datafiledatastarttimeutc, datafiledataendtimeutc, datafiledatainterval, datafilestarttimelabel, datafilestoptimelabel, datafilemidtimelabel, datafilenumberofnormalcommentlines, datafilenumberofspecialcommentlines) VALUES (:dataFileName, :dataFileVolume, :dataFileNumberOfFileVolumes, :dataFileWorkingPath, :dataFileNullDataFlag, :dataCollectionStartDateUTC, :dataFileRevisionNum, :dataFileRevisionComment, :dataFileRevisionDateUTC, :dataFileDataStartTimeUTC, :dataFileDataEndTimeUTC, :dataFileDataInterval, :dataFileStartTimeLabel, :dataFileStopTimeLabel, :dataFileMidTimeLabel, :dataFileNumberOfNormalCommentLines, :dataFileNumberOfSpecialCommentLines)");
$dataFileInfoTabQuery->bindParam(':dataFileName', $dataFileName);
$dataFileInfoTabQuery->bindParam(':dataFileVolume', $dataFileVolume);
$dataFileInfoTabQuery->bindParam(':dataFileNumberOfFileVolumes', $dataFileNumberOfFileVolumes);
$dataFileInfoTabQuery->bindParam(':dataFileWorkingPath', $dataFileWorkingPath);
$dataFileInfoTabQuery->bindParam(':dataFileNullDataFlag', $dataFileNullDataFlag);
$dataFileInfoTabQuery->bindParam(':dataCollectionStartDateUTC', $dataCollectionStartDateUTC);
$dataFileInfoTabQuery->bindParam(':dataFileRevisionNum', $dataFileRevisionNum);
$dataFileInfoTabQuery->bindParam(':dataFileRevisionComment', $dataFileRevisionComment);
$dataFileInfoTabQuery->bindParam(':dataFileRevisionDateUTC', $dataFileRevisionDateUTC);
$dataFileInfoTabQuery->bindParam(':dataFileDataStartTimeUTC', $dataFileDataStartTimeUTC);
$dataFileInfoTabQuery->bindParam(':dataFileDataEndTimeUTC', $dataFileDataEndTimeUTC);
$dataFileInfoTabQuery->bindParam(':dataFileDataInterval', $dataFileDataInterval);
$dataFileInfoTabQuery->bindParam(':dataFileStartTimeLabel', $dataFileStartTimeLabel);
$dataFileInfoTabQuery->bindParam(':dataFileStopTimeLabel', $dataFileStopTimeLabel);
$dataFileInfoTabQuery->bindParam(':dataFileMidTimeLabel', $dataFileMidTimeLabel);
$dataFileInfoTabQuery->bindParam(':dataFileNumberOfNormalCommentLines', $dataFileNumberOfNormalCommentLines);
$dataFileInfoTabQuery->bindParam(':dataFileNumberOfSpecialCommentLines', $dataFileNumberOfSpecialCommentLines);
$dataFileInfoTabQuery->execute();

// DATA VARIABLE INFO
$dataVariableInfoTabQuery = $dbconn->prepare("INSERT INTO data_variable_info (datavariablename, datavariabledescription, datavariabletype, datavariablestandardnamecore, datavariablestandardnamefull, dataVariableStandardNameIndex, datavariableunclabel, datavariablellodlabel, datavariablemissingdataflag, dataVariablelloddataflag, datavariablellodvalue, datavariableulodvalue, datavariabledataflaglabel, datavariabledataflagtype, datafilemidtimelabel, datavariableoutputname) VALUES (:dataVariableName, :dataVariableDescription, :dataVariableType, :dataVariableStandardNameCore, :dataVariableStandardNameFull, :dataVariableStandardNameIndex, :dataVariableUNCLabel, :dataVariableLLODLabel, :dataVariableMissingDataFlag, :dataVariableLLODdataFlag, :dataVariableLLODValue, :dataVariableULODValue, :dataVariableDataFlagLabel, :dataVariableDataFlagType, :dataFileMidTimeLabel, :dataVariableOutputName)");
$dataVariableInfoTabQuery->bindParam(':dataVariableName', $dataVariableName);
$dataVariableInfoTabQuery->bindParam(':dataVariableDescription', $dataVariableDescription);
$dataVariableInfoTabQuery->bindParam(':dataVariableType', $dataVariableType);
$dataVariableInfoTabQuery->bindParam(':dataVariableStandardNameCore', $dataVariableStandardNameCore);
$dataVariableInfoTabQuery->bindParam(':dataVariableStandardNameFull', $dataVariableStandardNameFull);
$dataVariableInfoTabQuery->bindParam(':dataVariableStandardNameIndex', $dataVariableStandardNameIndex);
$dataVariableInfoTabQuery->bindParam(':dataVariableUNCLabel', $dataVariableUNCLabel);
$dataVariableInfoTabQuery->bindParam(':dataVariableLLODLabel', $dataVariableLLODLabel);
$dataVariableInfoTabQuery->bindParam(':dataVariableMissingDataFlag', $dataVariableMissingDataFlag);
$dataVariableInfoTabQuery->bindParam(':dataVariableLLODdataFlag', $dataVariableLLODdataFlag);
$dataVariableInfoTabQuery->bindParam(':dataVariableLLODValue', $dataVariableLLODValue);
$dataVariableInfoTabQuery->bindParam(':dataVariableULODValue', $dataVariableULODValue);
$dataVariableInfoTabQuery->bindParam(':dataVariableDataFlagLabel', $dataVariableDataFlagLabel);
$dataVariableInfoTabQuery->bindParam(':dataVariableDataFlagType', $dataVariableDataFlagType);
$dataVariableInfoTabQuery->bindParam(':dataFileMidTimeLabel', $dataFileMidTimeLabel);
$dataVariableInfoTabQuery->bindParam(':dataVariableOutputName', $dataVariableOutputName);
$dataVariableInfoTabQuery->execute();

header('Location: ..');
























 ?>
