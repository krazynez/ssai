<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>ERROR!</title>
    <style>
      body {
        background-color: #545353;
      }

      .container {
        text-align: center;
      }

      button {
        border: none;
        font-size: 18px;
        border-radius: 5px;
        background-color: #1ad532;
        color: #FFFFFF;
        box-shadow: 2px 3px #454444;

      }

      button:hover {
          opacity: 0.8;
          font-size: 28px;
          cursor: pointer;
      }

    </style>
  </head>
  <form class="" action='<?=htmlentities("../");?>' method="post">

  <body>
    <div class="container">

    <h1 style="color:#FF0000; font-size: 72px; text-shadow: 3px 5px #372727">ERROR!</h1>
    <p style="color:#FFFFFF;">Something went wrong...</p>
    <button type="submit" name="goback">Go Back</button>


  </div>



  </body>
</form>
</html>
