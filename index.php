<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="../css/login.css">
  </head>
  <body>
  <form class="loginForm" action='<?=htmlentities("users/index.php");?>' method="post">



    <div class="container">
      <label for="username"></strong>Username</strong></label>
      <input type="text" placeholder="Enter Username" name="uname" required>

      <label for="pswd"></strong>Password</strong></label>
      <input type="password" placeholder="Enter Password" name="pswd" required>


      <button type="submit">Login</button>
      <label>
        <input type="checkbox" checked="checked" name="remember"> Remember me
      </label>
    </div>



    <div class="container" style="background-color:#F1F1F1">
      <button type="button" class="cancelbtn">Cancel</button>
      <span class="pswd">Forgot <a href="#">password?</a></span>

    </div>


</form>



  </body>
</html>
